import { useForm } from "react-hook-form"
import { translationAdd } from "../../api/translation"
import { HandleSENTENCE } from "../../views/Translation"
import { useUser } from "../../context/UserContext";
import { storageSave } from "../../utils/storage";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

const TranslationInput = () => {

    const { user, setUser } = useUser();
    const { register, handleSubmit } = useForm()

    const translationConfig = {
        required: true,
        minLength: 3,
        maxLength: 40
    }

    const onSubmit = async data => {
        HandleSENTENCE(data.translation);
        const [ error, updatedUser ] = await translationAdd(user, data.translation)
        if (error !== null){

        }
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser);
        console.log("Error: ", error);
        console.log("Result: ", updatedUser);

    }

    return (
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset>
                <label htmlFor="translation-input">Translate:</label>
                <input type="text" { ...register("translation", translationConfig) } placeholder="Word to translate" />
                <button type="submit">Translate</button>
            </fieldset>

        </form>
    )
}

export default TranslationInput