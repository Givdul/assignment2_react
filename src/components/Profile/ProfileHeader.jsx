const ProfileHeader = ({ username }) => {
    return (
        <header>
            <h2>Welcome back { username }</h2>
        </header>
    )
}
export default ProfileHeader;