import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";

const ProfileTranslationHistory = ({ translations }) => {

    const translationList = translations.map( 
        (translation, index  ) => <ProfileTranslationHistoryItem key={ index + "-" + translation } translation={ translation }/>)

    return (
       <section id="translation-history">
           <h4>Translation history: </h4>
           { translationList.length === 0 && <p>You have not translated anything yet.</p>}
           <ul>
                { translationList }
           </ul>
       </section>

    )
}
export default ProfileTranslationHistory;