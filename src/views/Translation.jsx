import TranslationInput from "../components/Translation/TranslationInput";
import withAuth from "../hoc/withAuth"

let SENTENCE = [];

export const HandleSENTENCE = (data) => {
    SENTENCE = [];
    for (const char of data){
        if (/[\W\d]/.test(char)){
            continue;
        }
        SENTENCE.push(char);
    }
    console.log(SENTENCE)
}


const Translation = () => {

    const sentenceOutput = SENTENCE.map(character => {
        return <img src={"img/"+ character + ".png"} alt="" key={character}/>
        
    })

    return (
      <>
        <h1>Translate:</h1>
        <section id="translation-input">
            <TranslationInput />
        </section>
        <section id="translation-output">
            { SENTENCE && sentenceOutput }
        </section>
      </>
    );
}
export default withAuth(Translation);