import './App.css';
import { BrowserRouter, Routes, Route} from "react-router-dom";
import Startup from './views/Startup';
import Translation from './views/Translation';
import Profile from './views/Profile';
import Navbar from './components/Navbar/Navbar';

function App() {
  return (
    <BrowserRouter>
        <Navbar />
          <div className="App">
            <Routes>
              <Route path="/" element={ <Startup /> } />
              <Route path="/Translation" element={ <Translation /> } />
              <Route path="/Profile" element={ <Profile /> } />
            </Routes>
        </div>
    </BrowserRouter>
  );
}

export default App;
