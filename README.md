# Lost in translation

The third assignment in the front end part of Noroff's Accelerate bootcamp is a react single page application where you can log in and translate words or short sentences to sign language.

## Links

* [Component tree](https://www.figma.com/file/BJP2YnulTMWNFpSYMCYhZr/ReactComponentTree?node-id=0%3A1)

* [App on Heroku](https://morning-falls-58997.herokuapp.com/)

## Installation

* Clone down the repository
* ``$npm install``
* ``$npm start``
* go to localhost url

## Usage

* Enter your username
* Translate
* When done logout to clear session storage

## Author
[Ludvig Hansen](https://gitlab.com/Givdul)

## License
[MIT](https://choosealicense.com/licenses/mit/)
